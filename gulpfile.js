#!/usr/bin/env node

var watch = require('gulp-watch');
var gulp = require('gulp');

const yargs = require("yargs");

const options = yargs
    .usage("Usage: -f <path> -t <to>")
    .option("f", { alias: "from", describe: "From path", type: "string", demandOption: true })
    .option("t", { alias: "to", describe: "To path", type: "string", demandOption: true })
    .argv;

gulp.task('myTask', function() {
    gulp.src(options.from + '/**/*', {base: options.from})
        .pipe(watch(options.from, {base: options.from}))
        .pipe(gulp.dest(options.to));
});

gulp.series('myTask')(function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log('success');
    }
});
