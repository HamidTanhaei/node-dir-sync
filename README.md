# Node Sync Directory
This package helps you to keep a directory files and folders sync based on another directory

`this version is the first version and not completed so it might have some problems.`

#### How to use
1- Install the package globally
```sh
$ npm install node-dir-sync -g
``` 

2- Start to watch
```sh
$ node-dir-sync -f /path/to/directory -t /path/to/directory
```